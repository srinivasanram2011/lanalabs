$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/featureFiles/lana_TC1.feature");
formatter.feature({
  "name": "Automate Lana labs UI Application",
  "description": "  Description :",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Verify the filter functionality is working",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "enter the URL to launch the application",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.java:28"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify that login is successful",
  "keyword": "When "
});
formatter.match({
  "location": "MyStepdefs.java:31"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "choose a existing log from alert",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.java:34"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "select a country from filter attribute option",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.java:37"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify the filter is applied",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:40"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "filter a specific attribute from pie chart",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.java:43"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "verify the filtered case count is equal to expected case count",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:46"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "complete the test verification",
  "keyword": "Then "
});
formatter.match({
  "location": "MyStepdefs.java:49"
});
formatter.result({
  "status": "passed"
});
});