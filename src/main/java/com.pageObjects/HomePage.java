package com.pageObjects;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import java.util.List;


public class HomePage extends BaseTest {

    String countryLabel;
    String countryCount;
    String filteredCaseCountAtTopDisplay;
    @FindBy(how= How.XPATH,xpath="//*[@id=\"addFilterMenuButton\"]")
    static WebElement selectFilter;
    @FindBy(how = How.XPATH,xpath = "//*[contains(text(),\"EXIT\")]")
    static WebElement exitModelView;
    @FindBy(how = How.XPATH,xpath = "//div[@role=\"menu\"]//button[@id=\"AddAttributeFilterButton\"]")
    static WebElement addAttributeFilter;
    @FindBy(how=How.XPATH,xpath = "//input[@placeholder=\"Search\"]")
    static WebElement searchTextField;
    @FindBy(how=How.XPATH,xpath = "//*[@class=\"mat-row ng-star-inserted\"]")
    static WebElement selectSearchedList;
    @FindBy(how=How.XPATH,xpath = "//button[@class=\"mat-button mat-primary\"]")
    static WebElement saveButton;
    @FindBy(xpath = "//*[@class=\"mat-card-header-text\"]")
    static WebElement appiledCountyFilter;
    @FindBy(xpath = "(//*[local-name()='svg'])[3]//*[local-name()='g']//*[local-name()='path']")
    static List<WebElement> getPiechartValue;
    @FindBy(xpath="//*[@class=\"attrFilter-tooltip\"]//following::div[@class=\"attrCount\"]")
    static WebElement attributeCount;
    @FindBy(xpath = "//div[@class=\"attrLabel\"]")
    static WebElement getCountry;
   @FindBy(xpath = "((//*[local-name()='mat-card-content'])[2]//*[local-name()='span'])[1]")
   static  WebElement newCaseCountDetails;



    public void selectFilter(String filter){
        PageFactory.initElements( driver, HomePage.class );
        exitModelView.click();
        selectFilter.sendKeys( Keys.ENTER );
        addAttributeFilter.sendKeys( Keys.ENTER );
        searchTextField.isEnabled();
        searchTextField.sendKeys( filter );
        selectSearchedList.click();
        saveButton.click();


    }
    public void verifyCountryFilter(){
        if(!appiledCountyFilter.isDisplayed()){
          Assert.assertFalse( "No filter is found",appiledCountyFilter.isDisplayed() );
        }
        else{
            String appliedFilter= appiledCountyFilter.getText();
            Assert.assertEquals( "Verifying applied filter is displayed by getting the text",appliedFilter,"Country filter" );
        }

    }
    public void applyAttributeFromPieChart(String countryCaseCount){
        for(int i = 0; i <= getPiechartValue.size()-1; i++){
            Actions action=new Actions(driver );
            action.moveToElement( getPiechartValue.get( i ) ).build().perform();
            countryLabel=getCountry.getText();
            if(countryLabel.equals( countryCaseCount )){
                countryLabel=getCountry.getText();
                countryCount=attributeCount.getText();
                getPiechartValue.get( i ).click();
                break;
            }
        }

    }
    public void caseCount() {
        selectFilter.sendKeys( Keys.ENTER );
        selectFilter.sendKeys( Keys.ENTER );
        filteredCaseCountAtTopDisplay=newCaseCountDetails.getText();
        String result =filteredCaseCountAtTopDisplay.substring(8,11);
        Assert.assertEquals(countryCount,result );

    }
    public void tearAndDown(){
        driver.quit();
    }

}
