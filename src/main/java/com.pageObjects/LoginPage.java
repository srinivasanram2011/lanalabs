package com.pageObjects;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;
import static org.junit.Assert.assertTrue;

public class LoginPage extends BaseTest {

    @FindBy(how= How.XPATH,xpath="(//*[@id=\"signInFormUsername\"])[2]")
    static WebElement userID;
    @FindBy(how= How.XPATH,xpath="(//*[@id=\"signInFormPassword\"])[2]")
    static WebElement pwd;
    @FindBy(how= How.XPATH,xpath="(//*[@name=\"signInSubmitButton\"])[2]")
    static WebElement loginSubmit;
    @FindBy(how=How.XPATH,xpath = "(//*[@role=\"dialog\"])[1]")
    static WebElement alertForLog;
    @FindBy(how= How.XPATH,xpath="//*[@class=\"mat-radio-outer-circle\"]")
    static WebElement selectRadioButton;
    @FindBy(how = How.XPATH,xpath = "//*[contains(text(),\"Use selected\")]")
    static WebElement useSelectedLog;


    public void initialRequest(String browser)
    {

        switch (browser){
            case "CHROME":
                WebDriverManager.getInstance(CHROME).setup();
                DesiredCapabilities acceptSSlCertificate = DesiredCapabilities.chrome();
                acceptSSlCertificate.setCapability( CapabilityType.ACCEPT_SSL_CERTS, true );
                driver = new ChromeDriver( acceptSSlCertificate );
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
                break;
            case "FIREFOX":
                WebDriverManager.firefoxdriver().setup();
                DesiredCapabilities acceptSSlCertificates = DesiredCapabilities.firefox();
                acceptSSlCertificates.setCapability( CapabilityType.ACCEPT_SSL_CERTS, true );
                driver = new FirefoxDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait( 10, TimeUnit.SECONDS );
                break;

        }
        PageFactory.initElements( driver, LoginPage.class );

    }
    public void launchApplication(String url, String userName, String password){
        initialRequest("CHROME");
        driver.get( url );
        userID.sendKeys(userName);
        pwd.sendKeys(password);
        loginSubmit.click();

    }
    public void verifyLogInSuccess(){
        waitForElement("(//*[@role=\"dialog\"])[1]");
        assertTrue("successfully logged in",alertForLog.isDisplayed());

    }
    public void chooseALog(){
        selectRadioButton.click();
        useSelectedLog.click();
    }
    public void waitForElement(String path){
        WebDriverWait wait=new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
    }
}
