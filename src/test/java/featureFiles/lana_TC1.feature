Feature:Automate Lana labs UI Application

  Scenario: Verify the filter functionality is working
    Given enter the URL to launch the application
    When verify that login is successful
    And choose a existing log from alert
    And select a country from filter attribute option
    Then verify the filter is applied
    And filter a specific attribute from pie chart
    Then verify the filtered case count is equal to expected case count
    Then complete the test verification










