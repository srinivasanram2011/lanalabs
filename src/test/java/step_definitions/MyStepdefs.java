package step_definitions;

import com.pageObjects.HomePage;
import com.pageObjects.LoginPage;
import cucumber.api.java8.En;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;


public class MyStepdefs implements En {

    private String URL;
    private String userName;
    private String password;

    public MyStepdefs() throws IOException {

        FileReader reader=new FileReader("src/main/java/com.testData/property.properties");
        Properties read=new Properties();
        read.load(reader);
        URL=read.getProperty("url");
        userName=read.getProperty("userName");
        password=read.getProperty("password");
        LoginPage loginPageObj=new LoginPage();
        HomePage homePageObj=new HomePage();

        Given( "^enter the URL to launch the application$", () -> {
            loginPageObj.launchApplication(URL,userName,password);
        } );
        When( "^verify that login is successful$", () -> {
            loginPageObj.verifyLogInSuccess();
        } );
        And( "^choose a existing log from alert$", () -> {
            loginPageObj.chooseALog();
        } );
        And( "^select a country from filter attribute option$", () -> {
            homePageObj.selectFilter("Country");
        } );
        Then( "^verify the filter is applied$", () -> {
            homePageObj.verifyCountryFilter();
        } );
        And( "^filter a specific attribute from pie chart$", () -> {
            homePageObj.applyAttributeFromPieChart("Austria");
        } );
        Then( "^verify the filtered case count is equal to expected case count$", () -> {
            homePageObj.caseCount();
        } );
        Then( "^complete the test verification$", () -> {
            homePageObj.tearAndDown();
        } );

    }
}
